package test;

public class Part {
	public String desc;
	public Double rate;
	public String getDesc() {
		return desc;
	}
	public Double getRate() {
		return rate;
	}
	private void input() {
		// TODO Auto-generated method stub

	}
	public Part() {
		// TODO Auto-generated constructor stub
	}
	public Part(String desc, Double rate) {
		super();
		this.desc = desc;
		this.rate = rate;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
}
