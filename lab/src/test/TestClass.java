package test;
import java.util.*;
import java.io.*;

class SortByPrice implements Comparator<Employee>{
	@Override
	public int compare(Employee e1,Employee e2){
		return (int) (e2.price - e1.price);
	}
}

class Employee implements Serializable{
	int bookId;
	String title;
	String authorName;
	String subject;
	double price;
	
	public Employee(int bookId, String title, String authorName, String subject, double price) {
		this.bookId = bookId;
		this.title = title;
		this.authorName = authorName;
		this.subject = subject;
		this.price = price;
	}
	
	@Override
	public String toString() {
		
		return String.format("%-20d%-20s%-20s%-20s%-20.2f",this.bookId,this.title,this.authorName,this.subject,this.price);
	}

}

class ListCOl {
	public List<Employee> list1;

	public void setList1(List<Employee> list1) {
		this.list1 = list1;
	}

	public void maxElement(){
		System.out.println(this.list1.get(0).price);
	}

	public void acceptStreamData() throws Exception {
		try (BufferedReader bReader = new BufferedReader(new InputStreamReader(System.in))){
			String t = bReader.readLine(); 
			while(t!=null && t.length()!= 0) {
				String[] t1 = t.split(",");

			this.list1.add(new Employee(Integer.parseInt(t1[0]),t1[1],t1[2],t1[3],Double.parseDouble(t1[4])));
				t = bReader.readLine();
			}
		} 
	}

	public void display() {
		for (Employee employee : list1) {
			System.out.println(employee.toString());
		}
	}
}

public class TestClass {
    public static void main(String args[] ) throws Exception {
        ListCOl listCOl = new ListCOl();
		listCOl.setList1(new ArrayList<Employee>());
		try {
			listCOl.acceptStreamData();
//			listCOl.display();
			listCOl.list1.sort(new SortByPrice());
			listCOl.maxElement();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

    }
}